/*
 * Copyright © 2014, Janne Grunau
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <sched.h>

#define PMUSERENR_EL0_EN (1 << 0)
#define PMUSERENR_EL0_CR (1 << 2)

static int test_pmccntr_access(void)
{
    uint32_t en;

    __asm__ volatile("MRS %[en], PMUSERENR_EL0" : [en]"=r"(en) ::);

    printf("PMUSERENR_EL0: %04x\n", en);

    return en & (PMUSERENR_EL0_EN | PMUSERENR_EL0_CR);
}

static int enable_pmccntr(void)
{
    uint64_t cfg, cen;

    __asm__ volatile("MRS %[cfg], PMCR_EL0         \n\t"
                     "MRS %[cen], PMCNTENSET_EL0   \n\t"
                     "ORR %[cfg], %[cfg], 1        \n\t"
                     "ORR %[cen], %[cen], 1<<31    \n\t"
                     "MSR PMCNTENSET_EL0, %[cen]   \n\t"
                     "MSR PMCR_EL0, %[cfg]         \n\t"
                     : [cfg]"=&r"(cfg),
                       [cen]"=&r"(cen)
                     ::);

    return cfg & 1;
}

static void read_pm_config_reg()\
{
    uint64_t cfg;

    __asm__ volatile("MRS %[cfg], PMCR_EL0" : [cfg]"=r"(cfg) ::);

    printf("config: %"PRIx64"\n", cfg);
}

static inline uint64_t read_ccntr(void)
{
    uint64_t c = 17;

    __asm__ volatile("MRS %[c], PMCCNTR_EL0" : [c]"=r"(c) ::);

    return c;
}

int enable_on_cpu(void)
{
    uint64_t ccntr, userenr;

    if (!(userenr = test_pmccntr_access())) {
        printf("No EL0 access to the cycle counter register\n");
        return 1;
    }

    if (userenr & PMUSERENR_EL0_EN) {
        read_pm_config_reg();
    }

    ccntr = read_ccntr();
    printf("ccntr: %"PRId64"\n", ccntr);

    if (userenr & PMUSERENR_EL0_EN) {
        enable_pmccntr();
    }

    if (userenr & PMUSERENR_EL0_EN) {
        read_pm_config_reg();
    }

    ccntr = read_ccntr();
    printf("ccntr: %"PRId64"\n", ccntr);

    return 0;
}

#define MAX_CPUS 32

int main(void)
{
    cpu_set_t mask;

    int err = sched_getaffinity(0, sizeof(mask), &mask);
    if (err) {
        printf("sched_getaffinity failed: %s\n", strerror(errno));
        return -1;
    }

    for (int c = 0; c < MAX_CPUS; c++) {
        if (CPU_ISSET(c, &mask)) {
            cpu_set_t set;
            CPU_ZERO(&set);
            CPU_SET(c, &set);
            err = sched_setaffinity(0, sizeof(set), &set);
            if (err) {
                printf("sched_setaffinity for CPU %d failed: %s\n", c, strerror(errno));
                return -1;
            }
            printf("Running on CPU %d\n", c);
            enable_on_cpu();
        }
    }

    return 0;
}
