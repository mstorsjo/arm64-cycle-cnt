// SPDX-License-Identifier: GPL-2.0-only
/*
 * enable user space access to the cycle counter
 *
 * Copyright (C) 2015 Janne Grunau
 */

#include "pmcnt.h"

void enable_ccnt_read(void* data)
{
	// PMUSERENR = 1
	asm volatile ("mcr p15, 0, %0, c9, c14, 0" :: "r"(1));
        // PMCR.E (bit 0) = 1
        asm volatile ("mcr p15, 0, %0, c9, c12, 0" :: "r"(1));
        // PMCNTENSET.C (bit 31) = 1
        asm volatile ("mcr p15, 0, %0, c9, c12, 1" :: "r"(1 << 31));
}
